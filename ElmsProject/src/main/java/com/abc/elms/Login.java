package com.abc.elms;

import java.util.ArrayList;

import org.apache.log4j.Logger;
/**
 * In this module all the login process will be executed 
 * @author ABC
 * @version Jdk 15
 *
 */
public class Login {
	private static final Logger LOGGER = Logger.getLogger(Login.class);

	// Validation of Admin
	public Admin checkadmin(String name, String password) {
		try {
			if (name.equalsIgnoreCase("Admin") && password.equals("123")) {

				LOGGER.info("Welcome  " + name + "\n");
				return new Admin();
			} else {
				LOGGER.warn("Invalid Username/Password");
			}
			return null;
		} catch (Exception e) {
			LOGGER.error("Some problem occured in checkadmin()" + e );
		}
		return null;
		
	}

	// Validation of Reporting Manager
	 ReportingManager matchRA(String name, String password, ArrayList<ReportingManager> rmdetails) {
		try {
			for (ReportingManager getd : rmdetails) {
				if (name.equalsIgnoreCase(getd.getRmname()) && password.equals(getd.getRmpwd())) {

					return getd;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Some problem occured in matchRA()" + e );
		}
		 

		return null;
	}

	// Validation of Regular Employee
	 Employee matchRE(String name, String password, ArrayList<Employee> redetails) {
		 try {
			 for (Employee getd : redetails) {
					if (name.equalsIgnoreCase(getd.getRename()) && password.equals(getd.getRepass())) {

						return getd;
					}
				}
		} catch (Exception e) {
			LOGGER.error("Some problem occured in matchRE()" + e );
		}
		
		return null;
	}

}
