package com.abc.elms;

import java.util.ArrayList;
import java.util.Scanner;

import org.apache.log4j.Logger;
/**
 * As soon as the application runs admin have to login using the credential 
 * once admin logged in then admin can add the ReportingManager/Employee 
 * 1 : can add Reportig Manager
 * 2 : can add Regular Employee
 * 3 : Add Employee
 * 4 : can view report of registered Reporting Manager
 * 5 : can view report of registered Reporting Employee
 * @author ABC
 * @version Jdk 15
 *
 */
public class Admin {
	Scanner scan = new Scanner(System.in);
	int total = 0;
	private static final Logger LOGGER = Logger.getLogger(Admin.class);

	// it adds Reporting Manager
	 void addRA(ArrayList<ReportingManager> rmdetails, int total)

	{
		 try {
			 String scanname;
				String scanpassword;

				for (int i = 0; i < total; i++) {

					LOGGER.info("\t\t\t Reporting Manager");
					LOGGER.info("-----------------------------------------------------");
					LOGGER.info("Enter name:");
					scanname = scan.next();
					boolean v = validatera(scanname, rmdetails);
					if (v != true) {
						LOGGER.info("Enter Password:");
						scanpassword = scan.next();

						rmdetails.add(new ReportingManager(scanname, scanpassword));
						LOGGER.info("Reporting Manager Details Added Successfully \n");
				
					} else {
						break;

					}
				}
		} catch (Exception e) {
			LOGGER.error("Some problem occured in addRA() method " + e);
		}
		
	}

	// it checks whether the new entered Reporting Manager exists or not
	private boolean validatera(String scanname, ArrayList<ReportingManager> rmdetails) {
		try {
			for (ReportingManager getd : rmdetails) {

				if (scanname.equalsIgnoreCase(getd.getRmname()))

				{
					LOGGER.info("Employee already exists please Enter again\n");
					return true;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Some problem occured in validatera() method " + e);
		}
		
		return false;
	}

	// it adds regular employee
	    void addRE(ArrayList<Employee> redetails, ArrayList<ReportingManager> rmdetails, int total)
	    {
		  try {
			  String scanname;
				String scanpassword;

				for (int i = 0; i < total; i++) {
			
					LOGGER.info("\t\t\t Employee Details ");
					LOGGER.info("Enter name:");
					scanname = scan.next();
					LOGGER.info("Enter Password:");
					scanpassword = scan.next();
					boolean v = validatere(scanname, scanpassword, redetails);

					if (v == true) {
						break;
					} else {

						int leave = 10;
						int used = 0;
						int req = 0;
						LOGGER.info("\t\t\tReporting Managers LIST \n");
						LOGGER.info("--------------------------------");

						for (ReportingManager getd : rmdetails) {
							if (getd.getRmname() != null) {
								LOGGER.info("Name is " + getd.getRmname());
							}
						}
						LOGGER.info("\n\nAssign Reporting Manager");
						LOGGER.info("\n\n Enter the name of the Reporting Manager to assign");
						String ment;
						String mentor = scan.next();

						for (ReportingManager getd : rmdetails) {

							if (mentor.equalsIgnoreCase(getd.getRmname())) {

								ment = getd.getRmname();
								redetails.add(new Employee(scanname, scanpassword, ment, used, req, leave));
								LOGGER.info("New Employee Created \n");

							}
						}

					}
				}
		} catch (Exception e) {
			LOGGER.error("Some problem occured in addRE() method " + e);
		}
		
	}

	// it checks whether the new entered employee exists or not
	private boolean validatere(String scanname, String scanpassword, ArrayList<Employee> redetails) {
		try {
			for (Employee getd : redetails) {

				if (scanname.equalsIgnoreCase(getd.getRename()) && scanpassword.equalsIgnoreCase(getd.getRepass())) {
					LOGGER.warn("Employee already exists please Enter again with different id or password\n");
					return true;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Some problem occured in validatere() method " + e);
		}
		
		return false;
	}

	// This function prints list of Reporting Managers with password
	 void viewra(ArrayList<ReportingManager> rmdetails) {
        try {
        	LOGGER.info("\t\t\tReporting Managers LIST\n");

    		for (ReportingManager getd : rmdetails) {
    			LOGGER.info("Name is  " + getd.getRmname() + "\t\tPassword is  " + getd.getRmpwd());

    		}
		} catch (Exception e) {
			LOGGER.error("Some problem occured in viewra() method " + e);
		}
		
	
	}

	// This function prints list of Regular Employee with password
	protected void viewre(ArrayList<Employee> redetails) {
      try {
    	  LOGGER.info("\t\t\tEmployee Details List \n");
  		LOGGER.info("-------------------------------------------------------------");

  		for (Employee getd : redetails) {
  			LOGGER.info("Name is  " + getd.getRename() + "\t\tPassword is  " + getd.getRepass()
  					+ "\t\tReportingManager: " + getd.getMentor());
  			

  		}
	} catch (Exception e) {
		LOGGER.error("Some problem occured in viewre() method " + e);
	}
		

	}

}
