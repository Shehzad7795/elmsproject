package com.abc.elms;

import java.util.Scanner;

import org.apache.log4j.Logger;

/**
 * In this Employee can 
 * 1 : can View Leave
 * 2 : Apply Leave
 * @author ABC
 * @version Jdk 15
 *
 */
public class Employee {

	private String rename;
	private String repass;
	private String mentor;
	private int releave;
	private int usedleave;
	private int requestl;
	Scanner scan = new Scanner(System.in);
	private static final Logger LOGGER = Logger.getLogger(Employee.class);
	

        //	This is an Employee Constructor which takes the data which is passed and store into instance variable 
        public Employee(String rename, String repass, String mentor, int usedleave, int requestl, int releave) {
		this.rename = rename;
		this.repass = repass;
		this.mentor = mentor;
		this.usedleave = usedleave;
		this.requestl = requestl;
		this.releave = releave;

	}



	/*
	 *  Displays available leaves
	 */
	public void viewleave() {
        try {
        	LOGGER.info("Your free leaves are:" + getReleave());
		} catch (Exception e) {
			LOGGER.error("Some problem occured in viewleave()" + e );
		}
		
	}

	/*
	 *  method for requesting leave
	 */
	public void reqleave(int request) {
      try {
    	// it checks if requested no. of leaves are available or not
  		if (request > getReleave()) {
  			
  			LOGGER.warn("You don't have sufficient free leaves\n");
  		} else {
  			LOGGER.info("Requested for " + request + "days");
  			setRequestl(request);
  		

  		}
	} catch (Exception e) {
		LOGGER.error("Some problem occured in reqleave() method " + e);
	}
		

	}
	
	
	
	/*
	 * Getters and Setters
	 */
    public String getRename() {
		return rename;
	}

	public void setRename(String rename) {
		this.rename = rename;
	}

	public String getRepass() {
		return repass;
	}

	public void setRepass(String repass) {
		this.repass = repass;
	}

	public String getMentor() {
		return mentor;
	}

	public void setMentor(String mentor) {
		this.mentor = mentor;
	}

	public int getReleave() {
		return releave;
	}

	public void setReleave(int releave) {
		this.releave = releave;
	}

	public int getUsedleave() {
		return usedleave;
	}

	public void setUsedleave(int usedleave) {
		this.usedleave = usedleave;
	}

	public int getRequestl() {
		return requestl;
	}

	public void setRequestl(int requestl) {
		this.requestl = requestl;
	}

	public void updateleave(int leave) {
		this.releave = leave;
	}

}
