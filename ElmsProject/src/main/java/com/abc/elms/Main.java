package com.abc.elms;

import java.util.ArrayList;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * 
 * This is an application for Employee Leave Management System 1 : ADD Employee.
 * 2 : can view report of registered Reporting Manager 3 : can view report of
 * registered Reporting Employee
 * 
 * @author ABC
 * @version Jdk 15
 *
 */
public class Main {
	Login l;
	private static final Logger LOGGER = Logger.getLogger(Main.class);
	static ArrayList<ReportingManager> rmdetails = new ArrayList<>();
	static ArrayList<Employee> redetails = new ArrayList<>();
    Admin admin= new Admin();
	// constructor for initializing objects
	Main() {
		l = new Login();
	}

	public static void main(String[] args) {
		// this is the main method
		try {
			Main leave = new Main();
			leave.display();
		} catch (Exception e) {
			LOGGER.error("Some problem occured in main() method " + e);
		}
		
	}

	// this is the display method
	 void display() {

		Scanner scan = new Scanner(System.in);
		String name;
		String password;
		int a = 0;
		int total = 0;
		PropertyConfigurator.configure("logger.properties");

		LOGGER.info("Select any one for login procedure  \n" + "1.Admin \n" + "2.Reporting Manager \n" + "3.Employee \n"
				+ "4.Exit");

		try {
			a = scan.nextInt();
			if (a == 4) {

				LOGGER.info("Thank You ");
				System.exit(0);
			}

			LOGGER.info("Enter Username or ID");
			name = scan.next();
			LOGGER.info("Enter Password");
			password = scan.next();
			switch (a) {
			case 1:
			   admin = l.checkadmin(name, password); // Login validation for Admin

				if (admin == null) {
					display();
				}
				while (true) {

					LOGGER.info("Choose one of the option given below : ");

					LOGGER.info("\nPress 1 : To ADD Employee." + "\n"
							+ "Press 2 :  To View Report of Reporting Managers" + "\n"
							+ "Press 3 : To View Report of Registered Employees" + "\n" + "Press 4 : Logout");

					a = scan.nextInt();
			

					switch (a) {

					case 1:

						LOGGER.info("\nPress 1 : To  Add Reporting Manager" + "\nPress 2 : To Add Regular Employee"
								+ "\nPress 3 : Logout");

						a = scan.nextInt();

						if (a == 1) { // Creates Reporting Manager

							LOGGER.info("How many employees you want to create??");
							total = scan.nextInt();
							admin.addRA(rmdetails, total);
							break;
						} else if (a == 2) { // Creates Regular Employee

							LOGGER.info("How many employees you want to create??");
							total = scan.nextInt();
							admin.addRE(redetails, rmdetails, total);
							break;
						} else if (a == 3) {
							display();
							break;
						} else {

							LOGGER.warn("Enter valid choice");
							break;
						}
					 

					case 2: // View report of Reporting Authority

						admin.viewra(rmdetails);

						break;
					case 3: 
						// View report of Regular Employee
						admin.viewre(redetails);

						break;
						
					case 4: 
						// display is called for logout
						display();
						break;
						
					default:
						break;
					}
                  break;
				}
			case 2:
				// Login validation for Manager
				ReportingManager rlogin = l.matchRA(name, password, rmdetails);

				if (rlogin != null) {

					LOGGER.info("Welcome" + "\t" + name + ",");
				} else {
					display();
					break;
				}
            
				while (true) {

					int c = 0;

					LOGGER.info("Choose options given below : ");
					LOGGER.info("------------------------------------------------");
					LOGGER.info("1.View Report\n2.View Requests\n3.Grant\n4.Logout");
					LOGGER.info("------------------------------------------------");
					c = scan.nextInt();
					if (c == 1) { // View his/her Employee's leave report

						rlogin.view(redetails);

						break;
					}

					else if (c == 2) { // View request of his/her allocated employee

						rlogin.requests(redetails);

						break;
					} else if (c == 3) { // Call the Grant method for Accepting or Rejecting or Pending request

						rlogin.grant(redetails);
						break;
					}

					else if (c == 4)
						display();
					break;

				}
				break;
			
				// Login validation for Employee
			case 3:
				Employee e = l.matchRE(name, password, redetails);

				if (e != null) {
					LOGGER.info("Welcome " + name + ",");
                     
					while (true) {
						int b = 0;
						int req = 0;
						LOGGER.info("----------------------------------------------------");
						LOGGER.info("1. View  Leaves" + "\n" + "2. Apply for leave application" + "\n" + "3. Logout");
						LOGGER.info("----------------------------------------------------");
						b = scan.nextInt();
						switch (b) {
						case 1: // case 1: Viewing his/her available leave
							LOGGER.info("Leaves Available");
							LOGGER.info("----------------------------------------------------");
							e.viewleave();
							LOGGER.info("----------------------------------------------------");
							break;

						case 2: // case 2: requesting leave
							LOGGER.info("----------------------------------------------------");
							LOGGER.info("Your total leaves are:" + e.getReleave());

							LOGGER.info("\n How many leaves do you want");
							req = scan.nextInt();
							e.reqleave(req);
							LOGGER.info("----------------------------------------------------");
							break;

						case 3: // case 2: Logout

							display();
							break;

						default:
							break;
						}
						break;
					}

				}
       
			default:
				LOGGER.warn("Enter valid number");
				display();
				break;
				
			}
		} 
		
		catch (Exception e) {
			LOGGER.error("Some problem occured in display() " + e);
			display();
		}
		scan.close();

	}
}
